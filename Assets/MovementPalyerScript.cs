﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPalyerScript :MonoBehaviour{

    private Animator animatorGracza;


    private float timer = 0;                    //licznikCzasu

    // Use this for initialization
    void Start()
    {
        animatorGracza = this.GetComponent<Animator>();//
        animatorGracza.Play("AimedWalk", -1);
    }

    // Update is called once per frame
    void Update(){
        timer += Time.deltaTime;

        if (timer > 5) {
            animatorGracza.Play("Rifle_Shoot", -1);
            animatorGracza.Play("AimedWalk", -1);
            timer = 0;
        }



    }
}
