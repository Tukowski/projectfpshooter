﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PackAmmoScript : MonoBehaviour {

    public int amountAmmoInPack = 60;

    void Update(){
        if (amountAmmoInPack<=0) {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other){
        if (other.tag.Equals("Player")) {
            //other.GetComponentInChildren<shootScript>().addAmmo(30);
            if (other.GetComponentInChildren<shootScript>().ammoAllAmount>=90) {
                //opis, że jest dużo amunicji.
            }
            if (other.GetComponentInChildren<shootScript>().ammoAllAmount<90) {
                int howMuchMiss = 0;
                howMuchMiss = 90 - other.GetComponentInChildren<shootScript>().ammoAllAmount;
                other.GetComponentInChildren<shootScript>().ammoAllAmount += howMuchMiss;
                amountAmmoInPack -= howMuchMiss;
            }
        }
    }
}
