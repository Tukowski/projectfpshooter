﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zombieScript : MonoBehaviour {

    public Animator anim;
    public objectHealth scriptHealth;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        scriptHealth = GetComponent<objectHealth>();
    }
	
	// Update is called once per frame
	void Update () {
        anim.SetInteger("walk", 1);

        if (scriptHealth.HpPoints <= 0) {
            anim.SetInteger("fallingback", 3);
        }
	}
}
