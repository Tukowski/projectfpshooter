﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class minionScript : MonoBehaviour {

    public Transform[] waypointsTable;
    public float speedMinion;
    public int current = 0;

    private void Awake(){
        this.transform.LookAt(waypointsTable[current]);
    }

    void Update () {
        if (this.transform.position != waypointsTable[current].position) {
            Vector3 positionThis = Vector3.MoveTowards(transform.position, waypointsTable[current].position, speedMinion * Time.deltaTime);
            GetComponent<Rigidbody>().MovePosition(positionThis);
        }
    }
    void OnTriggerEnter(Collider collisionWithWaypoint){
        Debug.Log("Kolizja z WP");
        this.transform.LookAt(waypointsTable[current]);
        current = (current + 1) % waypointsTable.Length;
    }
}
