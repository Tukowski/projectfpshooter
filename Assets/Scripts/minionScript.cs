﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyScript : MonoBehaviour {

    public Transform[] waypointsTable;
    public float speedMinion;
    public int current = 0;
    
	void Update () {
        if (this.transform.position != waypointsTable[current].position) {
            Vector3 positionThis = Vector3.MoveTowards(transform.position, waypointsTable[current].position, speedMinion * Time.deltaTime);
            GetComponent<Rigidbody>().MovePosition(positionThis);
            this.GetComponent<Animation>().Play("Walk");
        }
	}
    void OnTriggerEnter(Collider collisionWithWaypoint){
        Debug.Log("Kolizja z WP");
        current = (current + 1) % waypointsTable.Length;
    }



}
