﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class playerScript : MonoBehaviour {

	public float playerHpPoints = 100.0f;
	public float playerArmorPoints = 0.0f;

	public Text hpBar;
	public Text armorBar;
    

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		


		selfHit();

		hpBar.text = "HP: " + playerHpPoints;
		armorBar.text = "Armor: " + playerArmorPoints;

        if (playerHpPoints<=0) {
            Destroy(this.gameObject);
        }
	}

	public void playerHit(float dmgToThePlayer){
		if(playerArmorPoints>0){
			playerArmorPoints -= dmgToThePlayer;
			if(playerArmorPoints<0){
				playerHpPoints += playerArmorPoints;
				playerArmorPoints = 0;
			}
		}else{
			playerHpPoints -= dmgToThePlayer;
		}
	}





	public void playerHeal(float healToThePlayer){
		if(playerHpPoints<100)	playerHpPoints += healToThePlayer;
	}

	public void selfHit(){
		if (Input.GetKeyDown(KeyCode.X))
			playerHit(30);
	}
}
