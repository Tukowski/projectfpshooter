﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tooltips : MonoBehaviour {
    
	// Use this for initialization
	public void setActiveTRUE(){
        this.gameObject.SetActive(true);
    }

    public void setActiveFALSE(){
        this.gameObject.SetActive(false);
    }
}
