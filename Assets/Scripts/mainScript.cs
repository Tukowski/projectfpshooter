﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mainScript : MonoBehaviour {

	public float playerHpPoints;
	public int playerScorePoints;
	public int mapLevel;
	public float playerDamage;

	void Start () {
		playerHpPoints = 100;
		playerDamage = 10;
		playerScorePoints = 0;
		mapLevel = 0;
	}

	void Update () {
		
	}


	public void changeMapLevel (int giveMeLevel){
		SceneManager.LoadScene(giveMeLevel);
	}
	public void subtractPlayerHpPoints(float howMuchHpPointsLost){
		playerHpPoints -= howMuchHpPointsLost;
	}
	public void additionPlayerScorePoints(int howMuchPointsAdd){
		playerScorePoints += howMuchPointsAdd;
	}

}
