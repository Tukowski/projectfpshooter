﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class networkManagerScript : MonoBehaviour {

	public Text temp;
	public Text temp2;
	public RoomOptions roomOptions;
	public TypedLobby typedLobby;
	public bool isInRoom = false;
	// Use this for initialization
	void Start () {
		Connect();
		/*
		RoomOptions roomOptions = new RoomOptions();
		roomOptions.isOpen = true;
		roomOptions.maxPlayers = 10;
		*/

	}
	
	// Update is called once per frame
	void Update () {
		if(isInRoom==false)	{
			temp.text = PhotonNetwork.connectionStateDetailed.ToString();
		}else{
			temp.text = "Ilość graczy: " + PhotonNetwork.countOfPlayersOnMaster.ToString() + "\n" +
				"Ilość graczy w pokojach: " + PhotonNetwork.countOfPlayersInRooms.ToString() + "\n" +
				"Pokój: " + PhotonNetwork.room.ToString();

			string[] players = new string[PhotonNetwork.playerList.Length];
			string allPlayers = "Gracze \n";
			for(int x=0; x<PhotonNetwork.playerList.Length; x++){
				players[x] = PhotonNetwork.playerList.ToString();
				allPlayers += players [x]+"\n";
			}
			temp2.text = allPlayers;


		}
	}

	void Connect(){
		Debug.Log("Connect");
		PhotonNetwork.ConnectUsingSettings("projektFPS");
	}
	void OnConnectedToMaster(){
		Debug.Log("OnConnectedToMaster");
		OnJoinedLobby();
	}
	void OnJoinedLobby(){
		Debug.Log("OnJoinedLobby");
		JoinOrCreateRoom();
	}
	void JoinOrCreateRoom(){
		Debug.Log("JoinOrCreateRoom");
		PhotonNetwork.JoinOrCreateRoom("roomProjektFPS", roomOptions, typedLobby);
	}
	void OnJoinedRoom(){
		isInRoom = true;
		Debug.Log("Twój pokój to"+PhotonNetwork.room);
		PhotonNetwork.Instantiate("FPSController", new Vector3 (-5.0f, 4.0f, 7.0f), Quaternion.identity, 0);


	}



}
