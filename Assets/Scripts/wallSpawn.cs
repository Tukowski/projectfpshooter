﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wallSpawn : MonoBehaviour {

    public GameObject prefabWall;
    public Transform positionWall;
    public GameObject Walls;

    public float cooldownWalltime = 0f;
    public bool cooldownWall;

    void Start () {
        cooldownWall = false;
    }

    private void Awake(){
        
    }

    void Update () {

        if (cooldownWall) {
            cooldownWalltime -= Time.deltaTime;
            if (cooldownWalltime <= 0) {
                cooldownWalltime = 0f;
                cooldownWall = false;
            }
        }
        

        if (Input.GetKeyDown(KeyCode.T)) {
            if (!cooldownWall) {
                cooldownWalltime = 5f;
                cooldownWall = true;

                Debug.Log("playerWall - check key down T");
                GameObject playerWall = Instantiate(prefabWall, positionWall.position, positionWall.rotation) as GameObject;
                playerWall.transform.Rotate(new Vector3(45f, playerWall.transform.rotation.y, 0));
                playerWall.transform.parent = Walls.transform;
            }
            

        }
	}
}
