﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class shootScript : MonoBehaviour {

	public AudioClip audioShoot;
	public ParticleSystem effectShoot;
	public Texture2D crosshair2D;
	public Transform cameraTransformm;
	public Texture2D bloodTexture;
	public RaycastHit hit;
	public AudioSource source;
	public Text ammoBar;
    public Image ammoBarReloadTimer;

	public float shootVolume = 0.75f;
	public Vector3 lookingForward;
	public Rect crosshairPosition;
	public bool playerWasHit = false;
	public float opacity = 0.0f;
	public int ammoAmount = 30;
	public int ammoAllAmount = 90;
	public bool isReload = true;
	public float timer = 0.0f;
    
	void Start () {
		Cursor.visible = false;
		crosshairPosition = new Rect((Screen.width - crosshair2D.width) / 2,
			(Screen.height - crosshair2D.height) /2,
			crosshair2D.width,
			crosshair2D.height);
	
	}

	void Awake(){
		source = GetComponent<AudioSource>();
	}
    
	void Update () {
		lookingForward = transform.TransformDirection(Vector3.forward);

		if (isReload==true){
			if (Input.GetButtonDown("Fire1")) {
				source.PlayOneShot(audioShoot, shootVolume);
				effectShoot.Emit(20);
				ammoAmount--;
				if (Physics.Raycast(transform.position, lookingForward, out hit)) {
					if (hit.transform.tag == "Player") {
						Debug.Log("Player");
                        hit.collider.gameObject.GetComponent<playerScript>().playerHit(50);
                    }if (hit.transform.tag == "Enemy") {
                        hit.collider.gameObject.GetComponent<objectHealth>().objectHit(50);
                        Debug.Log("Enemy hit");
                    } else {
                        Debug.Log("nothing");
                    }
				}
			}
		}
		Vector3 forward = transform.TransformDirection(Vector3.forward) * 10;
		Debug.DrawRay(transform.position, forward, Color.green);

		ammoBar.text = ammoAmount + "/" + ammoAllAmount;

		if(ammoAmount<=0){
			isReload = false;
		}
        if (Input.GetKeyDown(KeyCode.R)) {
            isReload = false;
        }
		if(isReload==false) {
			timer += Time.deltaTime;
            ammoBarReloadTimer.fillAmount = timer / 3;
			if(timer >= 3) {
				int needAmmo = 30 - ammoAmount;

				if(ammoAllAmount >= needAmmo) {
					ammoAmount = 30;
					ammoAllAmount -= needAmmo;
				} else {
					ammoAmount += ammoAllAmount;
					ammoAllAmount = 0;
				}

				isReload = true;
				timer = 0.0f;
			}
		}

	}
	void OnGUI(){
		GUI.DrawTexture(crosshairPosition, crosshair2D);

		if (Input.GetKey (KeyCode.X)) {
			playerWasHit = true;
			opacity = 1.0f;
		}

		if(playerWasHit) {
			GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, opacity);
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), bloodTexture, ScaleMode.ScaleToFit);
			StartCoroutine("waitAndChangeOpacity");
		}

		if (opacity <= 0) {
			playerWasHit = false;
		}
	}
	//nie moje"
	IEnumerator waitAndChangeOpacity()
	{
		yield return new WaitForEndOfFrame();
		opacity -= 0.05f;
	}

    public void addAmmo(int howMuchAmmoAdd) {
        ammoAllAmount += howMuchAmmoAdd;
    }
}


/*





*/